package tk.vexisu.scrsht;

import tk.vexisu.scrsht.gui.LoginFrame;
import tk.vexisu.scrsht.gui.Notification;
import tk.vexisu.scrsht.server.Connection;
import tk.vexisu.scrsht.utils.OSValidator;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Properties;

/**
 * Created by Maciek on 2016-07-10.
 */
public class Main {

    private static String version = "0001";
    private static OSValidator os;
    private static LoginFrame loginFrame;
    private static Connection connection;
    private static Properties properties = new Properties();
    private static boolean shooting = false;
    private static BufferedImage recentscr = null;

    public static void main(String[] args){

        os = new OSValidator();

        if(os.getOS() == OSValidator.OS.MAC_OS){
            System.setProperty("apple.awt.UIElement", "true");
        }

        if(os.getOS() == OSValidator.OS.LINUX){
            try {
                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (UnsupportedLookAndFeelException e) {
                e.printStackTrace();
            }
        }

        if(!SystemTray.isSupported()){
            System.exit(0);
        }

        try {
            properties.load(new FileInputStream("bin/config.properties"));
        } catch (IOException e) {
        }

        connection = new Connection("52.59.242.47", 1507);

        if(connection.isConnected()){
            try {
                String out = getConnection().getIn().readUTF();
                if(!out.equals("version: "+ version)){
                    Notification n = new Notification(ImageIO.read(new File("bin/icon.png")),"scrSHT","Client is outdated!");
                    n.showNotification();
                    Thread.sleep(10000);
                    System.exit(1);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            loginFrame = new LoginFrame();
        }   else{
            try {
                Notification n = new Notification(ImageIO.read(new File("bin/icon.png")),"scrSHT","Server is not available!");
                n.showNotification();
                Thread.sleep(10000);
                System.exit(1);
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }

        }

    }

    public static void updateProperties(){
        File f = new File("bin/config.properties");
        OutputStream out = null;
        try {
            out = new FileOutputStream( f );
            Main.getProperties().store(out, "Config file for scrSHT.");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static boolean isShooting() {
        return shooting;
    }

    public static void setShooting(boolean shooting) {
        Main.shooting = shooting;
    }

    public static Connection getConnection() {
        return connection;
    }

    public static Properties getProperties() {
        return properties;
    }

    public static String getVersion() {
        return version;
    }

    public static BufferedImage getRecentscr() {
        return recentscr;
    }

    public static void setRecentscr(BufferedImage recentscr) {
        Main.recentscr = recentscr;
    }
}
