package tk.vexisu.scrsht.utils;

public class OSValidator {
    public enum OS{
        WINDOWS(0), MAC_OS(1), LINUX(2), SOLARIS(3);

        OS(int i) {
        }
    }

    OS os;

    public OSValidator(){
        String osname = System.getProperty("os.name").toLowerCase();
        if(osname.contains("win")){
            os = OS.WINDOWS;
        }
        if(osname.contains("mac") || osname.contains("darwin")){
            os = OS.MAC_OS;
        }
        if(osname.contains("nix") || osname.contains("nux") || osname.contains("aix")){
            os = OS.LINUX;
        }
        if(osname.contains("sunos")){
            os = OS.SOLARIS;
        }
    }

    public OS getOS() {
        return os;
    }
}
