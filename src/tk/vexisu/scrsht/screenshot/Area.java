package tk.vexisu.scrsht.screenshot;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by Maciek on 2016-07-10.
 */
public class Area extends JPanel {

    int xp, yp, xx ,yy;
    BufferedImage scr;
    boolean border = true;

    public Area(BufferedImage scr){
        super();
        this.scr = scr;
        this.xp = 0;
        this.xx = 0;
        this.yp = 0;
        this.yy = 0;
        setOpaque(true);
        setBackground(new Color(0,0,0,0));
    }

    public boolean isBorder() {
        return border;
    }

    public void setBorder(boolean border) {
        this.border = border;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        g.drawImage(scr, 0, 0, null);
        if(border){
            if(xp<=xx && yp <= yy){
                g.setColor(new Color(0,0,0));
                g.drawRect(xp-1,yp-1,xx-xp+2,yy-yp+2);
                g.setColor(new Color(248,105,73));
                g.drawRect(xp-2,yp-2,xx-xp+4,yy-yp+4);
                //g.setColor(new Color(255,255,255,50));
                //g.fillRect(xp,yp,xx-xp,yy-yp);
            }
            if(xp>=xx && yp >= yy){
                g.setColor(new Color(0,0,0));
                g.drawRect(xx-1,yy-1, xp-xx+2,yp-yy+2);
                g.setColor(new Color(248,105,73));
                g.drawRect(xx-2,yy-2,xp-xx+4,yp-yy+4);
                //g.setColor(new Color(255,255,255,50));
                //g.fillRect(xx,yy,xp-xx,yp-yy);
            }
            if(xp>=xx && yp <= yy){
                g.setColor(new Color(0,0,0));
                g.drawRect(xx-1,yp-1,xp-xx+2,yy-yp+2);
                g.setColor(new Color(248,105,73));
                g.drawRect(xx-2,yp-2,xp-xx+4,yy-yp+4);
                //g.setColor(new Color(255,255,255,50));
                //g.fillRect(xx,yp,xp-xx,yy-yp);
            }
            if(xp<=xx && yp >= yy){
                g.setColor(new Color(0,0,0));
                g.drawRect(xp-1,yy-1,xx-xp+2,yp-yy+2);
                g.setColor(new Color(248,105,73));
                g.drawRect(xp-2,yy-2,xx-xp+4,yp-yy+4);
                //g.setColor(new Color(255,255,255,50));
                //g.fillRect(xp,yy,xx-xp,yp-yy);
            }
        }
    }

    public int getXp() {
        return xp;
    }

    public void setXp(int xp) {
        this.xp = xp;
    }

    public int getYp() {
        return yp;
    }

    public void setYp(int yp) {
        this.yp = yp;
    }

    public int getXx() {
        return xx;
    }

    public void setXx(int xx) {
        this.xx = xx;
    }

    public int getYy() {
        return yy;
    }

    public void setYy(int yy) {
        this.yy = yy;
    }
}
