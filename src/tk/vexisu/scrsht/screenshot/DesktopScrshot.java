package tk.vexisu.scrsht.screenshot;

import tk.vexisu.scrsht.Main;
import tk.vexisu.scrsht.gui.AppTray;
import tk.vexisu.scrsht.gui.Notification;
import tk.vexisu.scrsht.server.Uploader;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Maciek on 2016-07-10.
 */
public class DesktopScrshot {

    private static SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd_HH.mm.ss");

    public static void shoot(){
        if(Main.isShooting()){
            return;
        }
        Main.setShooting(true);
        try {
            Calendar now = Calendar.getInstance();
            Robot robot = new Robot();
            BufferedImage screenShot = robot.createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
            ImageIO.write(screenShot, "PNG", new File("screenshots/" + formatter.format(now.getTime()) + ".png"));
            Notification n = new Notification(ImageIO.read(new File("bin/icon.png")),"scrSHT", "Desktop has been captured!");
            Main.setRecentscr(screenShot);
            AppTray.getUploadlast().setEnabled(true);
            n.addMouseListener(new MouseListener() {
                @Override
                public void mouseClicked(MouseEvent e) {

                }

                @Override
                public void mousePressed(MouseEvent e) {
                    n.setVisible(false);
                    Uploader.upload(screenShot);
                    Main.setRecentscr(null);
                    AppTray.getUploadlast().setEnabled(false);
                }

                @Override
                public void mouseReleased(MouseEvent e) {

                }

                @Override
                public void mouseEntered(MouseEvent e) {
                    n.setNInfo("Upload screenshot...");
                    n.repaint();
                }

                @Override
                public void mouseExited(MouseEvent e) {
                    n.setNInfo("Desktop has been captured!");
                    n.repaint();
                }
            });
            n.showNotification();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Main.setShooting(false);
    }
}
