package tk.vexisu.scrsht.screenshot;

import tk.vexisu.scrsht.Main;
import tk.vexisu.scrsht.gui.AppTray;
import tk.vexisu.scrsht.gui.Notification;
import tk.vexisu.scrsht.server.Uploader;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Maciek on 2016-07-10.
 */
public class AreaScrshot {

    private static boolean scr = false;
    private static Area area;

    private static SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd_HH.mm.ss");

    public static void shoot() {
        if(Main.isShooting()){
            return;
        }
        Main.setShooting(true);
        try {
            Calendar now = Calendar.getInstance();
            Robot robot = new Robot();
            BufferedImage screenShot = robot.createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));

            JFrame frame = new JFrame();
            frame.setSize(Toolkit.getDefaultToolkit().getScreenSize());
            frame.setLocation(0,0);
            frame.setUndecorated(true);
            frame.setLayout(null);
            frame.setVisible(true);
            GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();

            if (gd.isFullScreenSupported()) {
                gd.setFullScreenWindow(frame);
            }

            area = new Area(screenShot);
            area.setLocation(0,0);
            area.setSize(Toolkit.getDefaultToolkit().getScreenSize());
            frame.add(area);

            frame.setCursor(Cursor.CROSSHAIR_CURSOR);
            scr = true;


            MouseAdapter ma = new MouseAdapter() {

                @Override
                public void mousePressed(MouseEvent e) {
                    area.setXp(e.getX());
                    area.setYp(e.getY());

                    Runnable runnable = new Runnable() {
                        @Override
                        public void run() {
                            while(scr){
                                area.repaint();
                                area.setXx((int) MouseInfo.getPointerInfo().getLocation().getX());
                                area.setYy((int) MouseInfo.getPointerInfo().getLocation().getY());
                                try {
                                    Thread.sleep(17);
                                } catch (InterruptedException e1) {
                                    e1.printStackTrace();
                                }
                            }
                        }
                    };
                    Thread thread = new Thread(runnable);
                    thread.start();
                }

                @Override
                public void mouseReleased(MouseEvent e) {
                    scr = false;
                    area.setBorder(false);
                    area.repaint();
                    if(e.getButton() == MouseEvent.BUTTON3){
                        if (gd.isFullScreenSupported()) {
                            gd.setFullScreenWindow(null);
                        }
                        frame.setVisible(false);
                        Notification n = null;
                        try {
                            n = new Notification(ImageIO.read(new File("bin/icon.png")),"scrSHT", "Capture has been cancelled!");
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        }
                        n.showNotification();
                        return;
                    }
                    if(e.getButton() == MouseEvent.BUTTON1){
                        Rectangle rarea = null;
                        if(area.getXp() <= area.getXx() && area.getYp() <= area.getYy()){
                            rarea = new Rectangle(area.getXp(),area.getYp(),area.getXx() - area.getXp(),area.getYy() - area.getYp());
                        }
                        if(area.getXp() >= area.getXx() && area.getYp() >= area.getYy()){
                            rarea = new Rectangle(area.getXx(),area.getYy(),area.getXp() - area.getXx(),area.getYp() - area.getYy());
                        }
                        if(area.getXp() >= area.getXx() && area.getYp() <= area.getYy()){
                            rarea = new Rectangle(area.getXx(),area.getYp(),area.getXp() - area.getXx(),area.getYy() - area.getYp());
                        }
                        if(area.getXp() <= area.getXx() && area.getYp() >= area.getYy()){
                            rarea = new Rectangle(area.getXp(),area.getYy(),area.getXx() - area.getXp(),area.getYp() - area.getYy());
                        }
                        Rectangle finalRarea = rarea;
                        Runnable runnable = new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    area.repaint();
                                    BufferedImage screenShot = robot.createScreenCapture(finalRarea);
                                    if (gd.isFullScreenSupported()) {
                                        gd.setFullScreenWindow(null);
                                    }
                                    frame.setVisible(false);
                                    ImageIO.write(screenShot, "PNG", new File("screenshots/" + formatter.format(now.getTime()) + ".png"));
                                    Main.setRecentscr(screenShot);
                                    AppTray.getUploadlast().setEnabled(true);
                                    Notification n = new Notification(ImageIO.read(new File("bin/icon.png")),"scrSHT", "Area has been captured!");
                                    n.addMouseListener(new MouseAdapter() {

                                        @Override
                                        public void mousePressed(MouseEvent e) {
                                            n.setVisible(false);
                                            Uploader.upload(screenShot);
                                            Main.setRecentscr(null);
                                            AppTray.getUploadlast().setEnabled(false);
                                        }

                                        @Override
                                        public void mouseEntered(MouseEvent e) {
                                            n.setNInfo("Upload screenshot...");
                                            n.repaint();
                                        }

                                        @Override
                                        public void mouseExited(MouseEvent e) {
                                            n.setNInfo("Area has been captured!");
                                            n.repaint();
                                        }
                                    });
                                    n.showNotification();
                                } catch (Exception e1) {
                                    e1.printStackTrace();
                                }
                            }
                        };
                        Thread thread = new Thread(runnable);
                        thread.start();
                    }
                }
            };
            frame.addMouseListener(ma);

        } catch (Exception e) {
            e.printStackTrace();
        }
        Main.setShooting(false);
    }
}
