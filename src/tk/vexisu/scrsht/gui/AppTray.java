package tk.vexisu.scrsht.gui;

import tk.vexisu.scrsht.Main;
import tk.vexisu.scrsht.screenshot.AreaScrshot;
import tk.vexisu.scrsht.screenshot.DesktopScrshot;
import tk.vexisu.scrsht.server.Uploader;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * Created by Maciek on 2016-07-10.
 */
public class AppTray {

    private static TrayIcon trayicon;

    private static MenuItem uploadlast;

    public static void create(){
        PopupMenu popMenu= new PopupMenu();


        MenuItem scrSHT = new MenuItem("scrSHT");
        popMenu.add(scrSHT);
        scrSHT.setEnabled(false);

        trayicon = new TrayIcon(Toolkit.getDefaultToolkit().getImage("bin/icon.png"), "scrSHT");
        trayicon.setImageAutoSize(true);
        try {
            SystemTray.getSystemTray().add(trayicon);
        } catch (AWTException e) {
            e.printStackTrace();
        }

        MenuItem scrSHTfolder = new MenuItem("My screenshots");
        popMenu.add(scrSHTfolder);
        scrSHTfolder.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Desktop.getDesktop().open(new File("screenshots"));
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });

        MenuItem scrSHTaccount = new MenuItem("Shared screenshots");
        popMenu.add(scrSHTaccount);
        scrSHTaccount.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
                    if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
                        try {
                            Main.getConnection().getOut().writeUTF("accesskey:");
                            String key = Main.getConnection().getIn().readUTF();
                            desktop.browse(new URL("http://vexisu.tk/login.php?key=" + key + "&redirect=scrsht-app").toURI());
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }
            }
        });

        uploadlast = new MenuItem("Upload recent screenshot");
        popMenu.add(uploadlast);
        uploadlast.setEnabled(false);
        uploadlast.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(Main.getRecentscr() != null){
                    Uploader.upload(Main.getRecentscr());
                    Main.setRecentscr(null);
                    uploadlast.setEnabled(false);
                }
            }
        });

        MenuItem capture = new MenuItem("Capture");
        popMenu.add(capture);
        capture.setEnabled(false);

        MenuItem capturedesktop = new MenuItem("...desktop");
        popMenu.add(capturedesktop);
        capturedesktop.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Thread.sleep(300);
                } catch (InterruptedException ee) {
                    ee.printStackTrace();
                }
                DesktopScrshot.shoot();
            }
        });

        MenuItem capturearea = new MenuItem("...area");
        popMenu.add(capturearea);
        capturearea.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Thread.sleep(300);
                } catch (InterruptedException ee) {
                    ee.printStackTrace();
                }
                AreaScrshot.shoot();
            }
        });

        popMenu.addSeparator();

        MenuItem settings = new MenuItem("Preferences...");
        popMenu.add(settings);
        settings.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new PreferencesFrame();
            }
        });

        MenuItem exit = new MenuItem("Exit");
        popMenu.add(exit);
        exit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        trayicon.setPopupMenu(popMenu);
    }

    public static TrayIcon getTrayicon() {
        return trayicon;
    }

    public static MenuItem getUploadlast() {
        return uploadlast;
    }
}
