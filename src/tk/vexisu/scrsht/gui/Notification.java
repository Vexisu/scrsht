package tk.vexisu.scrsht.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by Maciek on 2016-07-11.
 */
public class Notification extends JFrame {

    BufferedImage icon;
    String title;
    String info;

    public Notification(BufferedImage icon, String title, String info){
        super();
        this.icon = icon;
        this.title = title;
        this.info = info;
        setSize(350,50);
        setLocation((int)Toolkit.getDefaultToolkit().getScreenSize().getWidth() - 375,35);
        setUndecorated(true);
        setLayout(null);
        setType(javax.swing.JFrame.Type.UTILITY);
        setAlwaysOnTop(true);
    }

    public void showNotification(){
        Runnable r = new Runnable() {
            @Override
            public void run() {
                setVisible(true);
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    setVisible(false);
                }
                setVisible(false);
            }
        };
        Thread t = new Thread(r);
        t.start();
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(
                RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setRenderingHint(
                RenderingHints.KEY_TEXT_ANTIALIASING,
                RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        g2d.drawImage(this.icon, 10,10,30,30,null);
        g2d.setColor(Color.black);
        g2d.setFont(new Font("SansSerif", Font.BOLD, 14));
        g2d.drawString(title,50,21);
        g2d.setFont(new Font("SansSerif", Font.PLAIN, 14));
        g2d.drawString(info,50,37);
        g2d.setColor(new Color(248, 105, 73));
        g2d.fillRect(0,47,350,3);
    }

    public void setNTitle(String title) {
        this.title = title;
    }

    public void setNInfo(String info) {
        this.info = info;
    }
}
