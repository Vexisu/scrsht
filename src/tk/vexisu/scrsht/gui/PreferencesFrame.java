package tk.vexisu.scrsht.gui;

import tk.vexisu.scrsht.Main;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Maciek on 2016-08-04.
 */
public class PreferencesFrame extends JFrame{

    JButton logout;


    public PreferencesFrame(){
        super("scrSHT Preferences // Version: "+ Main.getVersion() );
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        setLayout(null);
        setSize(370,300);
        setResizable(false);
        setLocation((int) ((dimension.getWidth() - getWidth()) / 2),(int) ((dimension.getHeight() - getHeight()) / 2));
        getContentPane().setBackground(Color.white);
        setIconImage(Toolkit.getDefaultToolkit().getImage("bin/icon.png"));

        setupComponents();
        setupComponentsActions();
        setVisible(true);
    }

    private void setupComponents(){
        logout = new JButton("Logout!");
        logout.setSize(150,25);
        logout.setLocation(5, 5);
        add(logout);

    }

    private void setupComponentsActions(){
        logout.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Main.getProperties().setProperty("autologin", "false");
                Main.getProperties().setProperty("password", "");
                Main.getProperties().setProperty("username", "");
                Main.updateProperties();
                System.exit(0);
            }
        });
    }
}
