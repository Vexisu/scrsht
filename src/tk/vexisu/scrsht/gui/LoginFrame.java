package tk.vexisu.scrsht.gui;

import tk.vexisu.scrsht.Main;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Maciek on 2016-07-10.
 */
public class LoginFrame extends JFrame{

    JButton login;
    JTextField username;
    JPasswordField passwd;
    JTextArea usernameinfo;
    JTextArea passwdinfo;
    JTextArea reginfo;
    JCheckBox autologin;

    public LoginFrame(){
        super("scrSHT Beta");
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        setLayout(null);
        setSize(370,300);
        setResizable(false);
        setLocation((int) ((dimension.getWidth() - getWidth()) / 2),(int) ((dimension.getHeight() - getHeight()) / 2));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        getContentPane().setBackground(Color.white);
        setIconImage(Toolkit.getDefaultToolkit().getImage("bin/icon.png"));

        setupComponents();
        setupComponentsActions();
        setVisible(true);

        if(Main.getProperties().getProperty("autologin").equals("true")){
            autologin.setSelected(true);
            String susername = Main.getProperties().getProperty("username");
            username.setText(susername);
            String spassword = Main.getProperties().getProperty("password");
            autologin(susername, spassword);
        }
    }

    private void setupComponents(){
        JLabel logo = new JLabel(new ImageIcon("bin/logo.png"));
        logo.setLocation(15,15);
        logo.setSize(340,140);
        add(logo);


        login = new JButton("Login!");
        login.setSize(150,25);
        login.setLocation(22, 215);
        add(login);

        usernameinfo = new JTextArea("Username:");
        usernameinfo.setEditable(false);
        usernameinfo.setSize(65, 16);
        usernameinfo.setLocation(15,160);
        add(usernameinfo);

        passwdinfo = new JTextArea("Password:");
        passwdinfo.setEditable(false);
        passwdinfo.setSize(65, 16);
        passwdinfo.setLocation(15,185);
        add(passwdinfo);

        reginfo = new JTextArea("Not registered yet? \nClick here!");
        reginfo.setEditable(false);
        reginfo.setSize(150,50);
        reginfo.setLocation(200, 220);
        add(reginfo);

        username = new JTextField();
        username.setSize(250,20);
        username.setLocation(92,159);
        add(username);

        passwd = new JPasswordField();
        passwd.setSize(250,20);
        passwd.setLocation(92, 184);
        add(passwd);

        autologin = new JCheckBox("Login automatically");
        autologin.setSize(150,20);
        autologin.setLocation(22, 243);
        autologin.setBackground(new Color(255,255,255));
        autologin.setHorizontalTextPosition(SwingConstants.LEFT);
        add(autologin);


    }

    private void setupComponentsActions(){
        reginfo.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                super.mouseReleased(e);
                Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
                if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
                    try {
                        Thread.sleep(100);
                        desktop.browse(new URL("http://vexisu.tk/register.php").toURI());
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });
        login.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    Main.getConnection().getOut().writeUTF("usr: " + username.getText());

                    MessageDigest md = null;
                    try {
                        md = MessageDigest.getInstance("MD5");
                    } catch (NoSuchAlgorithmException e2) {
                    }
                    byte[] array = md.digest(new String(passwd.getPassword()).getBytes());
                    StringBuffer sb = new StringBuffer();
                    for (int i = 0; i < array.length; ++i) {
                        sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
                    }

                    Main.getConnection().getOut().writeUTF("pswd: " + sb.toString());
                    Main.getConnection().getOut().writeUTF("login:");

                    String result = Main.getConnection().getIn().readUTF();

                    if(result.equals("true:")){
                        setVisible(false);
                        AppTray.create();
                        if(autologin.isSelected()){
                            Main.getProperties().setProperty("username", username.getText());
                            Main.getProperties().setProperty("password", sb.toString());
                            Main.getProperties().setProperty("autologin", "true");
                            Main.updateProperties();
                        }
                    }
                    if (result.equals("err:")) {
                        Notification n = new Notification(ImageIO.read(new File("bin/icon.png")), "scrSHT", "Username or password is incorrect.");
                        n.showNotification();
                    }

                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });
    }

    private void autologin(String un, String pw){
        try {
            Main.getConnection().getOut().writeUTF("usr: " + un);
            Main.getConnection().getOut().writeUTF("pswd: " + pw);
            Main.getConnection().getOut().writeUTF("login:");

            String result = Main.getConnection().getIn().readUTF();

            if(result.equals("true:")){
                setVisible(false);
                AppTray.create();
            }
            if (result.equals("err:")) {
                Notification n = new Notification(ImageIO.read(new File("bin/icon.png")), "scrSHT", "Username or password is incorrect.");
                n.showNotification();
                Main.getProperties().setProperty("username", "");
                Main.getProperties().setProperty("password", "");
                Main.getProperties().setProperty("autologin", "false");
                Main.updateProperties();
            }

        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }
}
