package tk.vexisu.scrsht.server;

import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.net.Socket;

/**
 * Created by Maciek on 2016-07-22.
 */
public class DataConnection {

    private Socket socket;
    private DataInputStream in;
    private DataOutput out;
    private Boolean connected = false;

    public DataConnection(String ip, int port){
        try{
            socket = new Socket(ip, port);
            in = new DataInputStream(socket.getInputStream());
            out = new DataOutputStream(socket.getOutputStream());
            connected = true;
        } catch (Exception e){
        }
    }

    public Socket getSocket() {
        return socket;
    }

    public DataInputStream getIn() {
        return in;
    }

    public DataOutput getOut() {
        return out;
    }

    public Boolean isConnected() {
        return connected;
    }
}
