package tk.vexisu.scrsht.server;

import tk.vexisu.scrsht.Main;
import tk.vexisu.scrsht.gui.Notification;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * Created by Maciek on 2016-07-18.
 */
public class Uploader {

    public static boolean upload(BufferedImage scr) {

        try {
            Main.getConnection().getOut().writeUTF("upload:");
            String key = Main.getConnection().getIn().readUTF();
            DataConnection dataConnection = new DataConnection("52.59.242.47", 1508);
            dataConnection.getOut().writeUTF("key: "+ key);
            ImageIO.write(scr, "JPG", dataConnection.getSocket().getOutputStream());
            dataConnection.getSocket().getOutputStream().close();
            dataConnection.getSocket().close();
            Notification n = new Notification(ImageIO.read(new File("bin/icon.png")),"scrSHT", "Screenshot has been uploaded!");
            n.showNotification();
            Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
            if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
                try {
                    Thread.sleep(500);
                    desktop.browse(new URL("http://vexisu.tk/scrsht-scr.php?key=" + key).toURI());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

}
